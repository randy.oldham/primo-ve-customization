(function () {
    var app = angular.module('viewCustom', ['angularLoad']);
    "use strict";

/* Based heavily on https://ki.primo.exlibrisgroup.com/discovery/custom/46KIB_INST-46KIB_VU1/js/custom.js */
  app.component('prmSearchBookmarkFilterAfter', {
    template: '<a class="md-icon-button hide-sm button-over-dark md-button md-primoExplore-theme md-ink-ripple" id="chat-icon" aria-label="Chat" ng-href="{{$ctrl.getChatSkin()}}" target="_blank" rel="noopener noreferrer"><md-icon alt="" class="md-primoExplore-theme" aria-hidden="true"><svg width="100%" height="100%" viewBox="0 0 24 24" y="1056" xmlns="http://www.w3.org/2000/svg" fit="" preserveAspectRatio="xMidYMid meet" focusable="false"><g><path fill="none" d="M0,0h24v24H0V0z"/></g><g><path d="M4,4h16v12H5.17L4,17.17V4L4,4 M4,2C2.9,2,2.01,2.9,2.01,4L2,22l4-4h14c1.1,0,2-0.9,2-2V4c0-1.1-0.9-2-2-2 H4L4,2z"/><polygon points="6,12 14,12 14,14 6,14 6,12"/><polygon points="6,9 18,9 18,11 6,11 6,9"/><polygon points="6,6 18,6 18,8 6,8 6,6"/></g></svg></md-icon></a><prm-language-selection style="display: none"></prm-language-selection><button class="md-icon-button hide-sm button-over-dark md-button md-primoExplore-theme md-ink-ripple" id="lang-icon" ng-click="$ctrl.switchLang()"><div ng-class="$ctrl.getClass()"></div></button>',
    'controller': function controller($rootScope, $translate) {
      var vm = this;
      vm.lang = $translate.use();
      var otherLanguage = $translate.use() === 'fr' ? 'en' : 'fr';
      vm.switchLang = function () {
        if (!$rootScope.languageSelectionCtrl) {
          return false;
        }
        return $rootScope.languageSelectionCtrl.changeLanguage(otherLanguage);
      };
      vm.getClass = function () {
        return $translate.use() === 'fr' ? 'lang-en' : 'lang-fr';
      };
      vm.getChatSkin = function () {
        return $translate.use() == 'en' ? "https://ca.libraryh3lp.com/chat/laurentian@chat.ca.libraryh3lp.com?skin=15614&theme=simpletext&title=Chat+with+us!" : "https://ca.libraryh3lp.com/chat/laurentian-fr@chat.ca.libraryh3lp.com?lang=fr-ca&skin=15626&theme=fr-ca&title=Clavarder+avec+nous!";
      }

      var french = $translate.use() === 'fr' ? true : false;

      angular.element(document).ready(function() {
        var chatIcon = document.querySelector('#chat-icon'),
          langIcon = document.querySelector('#lang-icon'),
          chatLabel = french ? 'Clavarder avec nous!' : 'Chat with us!',
          langLabel = french ? 'Change language to English' : 'Changer la langue en français',
          contactLink = document.querySelector('a[href="https://biblio.laurentian.ca/research/contact-us"]'),
          showTooltip = function(e) {
            var position = e.target.getBoundingClientRect();
            var tooltip = document.createElement('md-tooltip');
            tooltip.className = 'menu-tooltip';
            tooltip.setAttribute('role', 'tooltip');
            tooltip.innerHTML = '<div class="_md-content _md _md-show"><span>' + e.target.getAttribute('aria-label') + '</span></div>';
            tooltip.style.top = position.top + e.target.offsetHeight + 8 + 'px';
            document.body.appendChild(tooltip);
            setTimeout(function() {
              tooltip.classList.add('_md-show');
              tooltip.style.left = position.left - (tooltip.offsetWidth - e.target.offsetWidth) / 2 +  'px';
            }, 500);
          },
          hideTooltip = function(e) {
            var tp = document.querySelectorAll('.menu-tooltip');
            if (tp.length) {
              Array.prototype.forEach.call(tp, function(el) {
                el.parentElement.removeChild(el);
              })
            }
          },
          setListeners = function(el) {
            el.addEventListener('mouseenter', showTooltip, false);
            el.addEventListener('focus', showTooltip, false);
            el.addEventListener('mouseleave', hideTooltip, false);
            el.addEventListener('focusout', hideTooltip, false);
            el.addEventListener('click', hideTooltip, false);
          };
        if (chatIcon) {
          chatIcon.setAttribute('aria-label', chatLabel);
          setListeners(chatIcon);
        }
        if (langIcon) {
          langIcon.setAttribute('aria-label', langLabel);
          langIcon.setAttribute('lang', $translate.use() === 'fr' ? 'en' : 'fr');
          setListeners(langIcon);
        }
        if (contactLink && french) {
          contactLink.href = 'https://biblio.laurentian.ca/research/fr/coordonn%C3%A9es-et-renseignements';
        }
      });
    },
    'bindings': { 'parentCtrl': '<' }
  });

  app.component('prmLanguageSelectionAfter', {
    'bindings': { 'parentCtrl': '<' },
    'controller': function controller($rootScope) {
      $rootScope.languageSelectionCtrl = this.parentCtrl;
    }
  });
})();
